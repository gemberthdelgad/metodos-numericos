boleto(manta,	quito,	hk_4817,	lunes,		[2, 3, 5, 4]).
boleto(quito,	cuenca,		hk_4812,	lunes,		[5]).
boleto(cuenca,	esmeraldas,	hk_4817,	miercoles,	[5, 6, 3, 7, 4]).
boleto(manta,	esmeraldas,		hk_4093,	sabado,		[6, 8, 2]).
boleto(cuenca,	puyo,	hk_3212,	jueves,		[4, 8, 9, 1, 2]).
boleto(machala,	salinas,	hk_3212,	viernes,	[2, 3, 6]).
boleto(esmeraldas, jipijapa,	hk_4812,	domingo,	[1, 4]).
boleto(quito,	salinas,	hk_4093,	martes,		[3, 2, 9, 5]).
boleto(cuenca,	jipijapa,	hk_3212,	viernes,	[7, 4]).

ciudad(manta,		1924,	calido,		6).
ciudad(quito,	1534,	templado,		2850).
ciudad(cuenca,		1577,	templado,	2560).
ciudad(machala,		1824,	calido,	6).
ciudad(esmeraldas,	1526,	tropical,		15).
ciudad(puyo,	1889,	tropical,	950).
ciudad(salinas,		1937,	calido,		8).

bus(hk_4817,	30,	hino).
bus(hk_4812,	60,	volvo).
bus(hk_3212,	30,	jac).
bus(hk_4093,	40,	mitsubishi).




% Reglas

% Obtener los destinos a los cuales se viajÃ³ desde manta
obtenerPuntoUno(Origen, Destino) :- boleto(Origen, Destino, _, _,_).


% Obtener las ciudades y altitud de las ciudades que son de clima calido.
obtenerPuntoDos(Clima, Ciudad, Altitud) :- ciudad(Ciudad, _, Clima, Altitud).


% Obtener la ciudad de origen, destino y pasajes, de los boletos realizado el viernes
obtenerPuntoTres(Dia, Origen, Destino, Pasaje) :- boleto(Origen, Destino, _, Dia, Pasaje).


% Obtener el nombre y clima de las ciudades a las que se viaje desde
% quito
obtenerPuntoCuatro(Origen, Destino, Clima) :- obtenerPuntoUno(Origen, Destino), ciudad(Destino, _, Clima, _).

% Obtener el nombre y clima de las ciudades a las que se viaja desde
% quito el dia jueves
obtenerPuntoCinco(Dia, Origen, Destino, Clima) :- obtenerPuntoTres(Dia, Origen, Destino, _), ciudad(Destino, _, Clima, _).

% La ciudad de origen, destino, altitud y numero de asientos, de los
% viajes realizados el lunes
obtenerPuntoSeis(Dia, Origen, Destino, Altitud, NumAsientos) :- boleto(Origen, Destino, Matricula, Dia, _),																ciudad(Destino, _, _, Altitud),																bus(Matricula, NumAsientos, _).


% La ciudad de origen, destino, altitud y nÃºmero de asientos, de los viajes realizados el lunes
obtenerPuntoSiete(Dia, Origen, Destino, Altitud, NumAsientos) :- boleto(Origen, Destino, Matricula, Dia, _),																ciudad(Destino, _, _, Altitud),
                                                                                         bus(Matricula, NumAsientos, _).
%consulta que utiliza la recursividad para obtener el nombre de la ciudad de origen, destino y clima de las ciudades a las que se viajÃ³ desde Machala el dia jueves
obtenerPuntoOcho(Dia, Origen, Destino, Clima) :- obtenerPuntoCinco(Dia, Origen, Destino, Clima), obtenerPuntoCuatro(Origen, Destino, Clima).

%obtener las ciudades que fueron fundadas antes de 1924 y que son de clima calido
obtenerPuntoNueve(Ciudad, Altitud) :- ciudad(Ciudad, Anio, Clima, Altitud), Anio < 1924, Clima == calido.



%cual es el origen de los pasajeros que solo ocupan un asientos
obtenerPuntoR(Origen) :- boleto(Origen, _, _, _, [_]).
% Cual es el destino lo los pajajeros que almenos an comprado dos
% hacientos
obtenerPuntoR1(Destino) :- boleto(_, Destino, _, _, [_,_|_]).
%contar los boletos segun indique el dia

%count([],0)
count(W,N):-count(W,T), N is T+1.
obtenerPuntoR2(Dia, W) :- boleto(_, _, _, Dia, [_|w]), count(W).
