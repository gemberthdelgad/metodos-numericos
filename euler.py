import numpy as np
import matplotlib.pyplot as plt
import math as m
#aqui se almacena la funcion dy/dx y la devuelve para ser usada más tard
funcion=lambda x,y:y*pow(x,2)-1.1*y
#Plus aque va la el resultado exacto  
#aqui se piden los datos como tamaño de paso (h)
#y hasta donde se quiere llegar, es decir el valor de xi 
h=float(input("Tamaño de paso:  "))
xi=float(input("Ingrese hasta donde llegara x:  "))

#cuantas iteraciones se requieren en la memoria
n=int((xi/h)+1)
#aqui se dan los valores iniciales
x=np.zeros(n)
y=np.zeros(n)
x[0]=0
y[0]=1
print ("\n",x[0],"       || ", y[0])
print('\n{:^10}         {:^10}           {:^10}           {:^10}   \n '.format('i','xi  ','yi','m=(dy/dx)=yx2-1.1y'))

#para mover atraves de los vectores 
#el arange permite moverse por el vector con valores decimales 
for i in np.arange(1,n):
    
    y[i]=y[i-1]+(funcion(x[i-1],y[i-1]))*h
    #tamaño de paso
    x[i]=x[i-1]+h
        
    print('{:^10}           {:^10.9f}            {:^10.9f}        {:^10.9f}  \n '.format(i,float( x[i]),float(y[i]),float(funcion(x[i-1],y[i-1]))))
   
  
   
plt.plot(x,y)
#plt.plot(x,ys,color='red')
plt.xlabel('Variable independiente,  x')
plt.ylabel('Variable dependiente, y')
plt.show()
